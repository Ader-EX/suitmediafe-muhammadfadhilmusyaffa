import React, { useEffect, useState } from "react";
import KeyboardDoubleArrowLeftIcon from "@mui/icons-material/KeyboardDoubleArrowLeft";
import KeyboardDoubleArrowRightIcon from "@mui/icons-material/KeyboardDoubleArrowRight";
import PaginationItem from "@mui/material/PaginationItem";
import Cards from "./Cards";
import axios from "axios";
import Pagination from "@mui/material/Pagination";
import Stack from "@mui/material/Stack";
import defaultImageUrl from "../assets/imgplaceholder.jpg";

export default function Ideas() {
  const [ideas, setIdeas] = useState([]);
  const [resultItem, setResultItem] = useState([]);
  const [isloading, setIsLoading] = useState(true);

  const [append, setAppend] = useState(["small_image"]);

  const [pageNumber, setPageNumber] = useState(
    Number(localStorage.getItem("pageNumber")) || 1
  );
  const [pageSize, setPageSize] = useState(
    Number(localStorage.getItem("pageSize")) || 10
  );
  const [sort, setSort] = useState(
    localStorage.getItem("sort") || "-published_at"
  );

  useEffect(() => {
    const fetchData = async () => {
      localStorage.setItem("pageNumber", pageNumber);
      localStorage.setItem("pageSize", pageSize);
      localStorage.setItem("sort", sort);
      const result = await axios.get(
        "https://suitmedia-backend.suitdev.com/api/ideas",
        {
          params: {
            "page[number]": pageNumber,
            "page[size]": pageSize,
            sort: sort,
            append: append.join(","),
          },
        }
      );

      setIdeas(result.data.data);
      setResultItem(result.data);
      console.log(result.data);
      setIsLoading(false);
    };

    fetchData();
  }, [pageNumber, pageSize, append, sort]);

  if (isloading) {
    return (
      <div className="flex items-center justify-center w-full mt-5 text-5xl font-bold">
        Loading...
      </div>
    );
  }

  return (
    <div className="p-8 ">
      <section className="flex flex-col justify-between w-full px-8 py-6 md:flex-row">
        <div>
          Showing {(pageNumber - 1) * pageSize + 1} -{" "}
          {Math.min(pageNumber * pageSize, resultItem.meta.total)} of{" "}
          {resultItem.meta.total}
        </div>
        <div className="flex flex-col gap-8 md:flex-row">
          <div className="">
            <label htmlFor="itemsPerPage">Show per Page : </label>
            <select
              value={pageSize}
              onChange={(e) => {
                setPageSize(Number(e.target.value));
              }}
              id="itemsPerPage"
              className="w-full px-4 py-3 mx-4 text-sm text-gray-900 border border-gray-300 md:w-20 bg-gray-50 rounded-xl"
            >
              <option value={10}>10</option>
              <option value={20}>20</option>
              <option value={50}>50</option>
            </select>
          </div>
          <div>
            <div>
              <label htmlFor="sortBy">Sort By : </label>
              <select
                id="sortBy"
                value={sort === "-published_at" ? "Newest" : "Oldest"}
                onChange={(e) => {
                  const value = e.target.value;
                  setSort(
                    value === "Newest" ? "-published_at" : "published_at"
                  );
                }}
                className="w-full px-4 py-3 mx-4 text-sm text-gray-900 border border-gray-300 md:w-40 bg-gray-50 rounded-xl"
              >
                <option value="Newest">Newest</option>
                <option value="Oldest">Oldest</option>
              </select>
            </div>
          </div>
        </div>
      </section>
      <div className="flex flex-wrap gap-8 mx-4 overflow-hidden">
        {ideas.map((idea, index) => (
          <Cards
            key={index}
            title={idea.title}
            imageUsed={
              idea.small_image && idea.small_image[0]
                ? idea.small_image[0].url
                : { defaultImageUrl }
            }
            publishedAt={idea.published_at}
          />
        ))}
      </div>
      <div className="flex items-center justify-center w-full ">
        <Stack spacing={2}>
          <Pagination
            count={resultItem.meta.last_page}
            showFirstButton
            showLastButton
            renderItem={(item) => {
              if (item.type === "first") {
                return (
                  <PaginationItem
                    {...item}
                    icon={<KeyboardDoubleArrowLeftIcon />}
                  />
                );
              }
              if (item.type === "last") {
                return (
                  <PaginationItem
                    {...item}
                    icon={<KeyboardDoubleArrowRightIcon />}
                  />
                );
              }
              return <PaginationItem {...item} />;
            }}
            onChange={(event, value) => {
              setPageNumber(value);
            }}
            sx={{
              "& .MuiPaginationItem-page": {
                fontWeight: "600",
              },
              "& .Mui-selected": {
                backgroundColor: "#FF6500 !important",
                color: "white !important",
                borderRadius: "8px !important",
                paddingY: "1.2rem !important",
              },
            }}
          />
        </Stack>
      </div>
    </div>
  );
}
