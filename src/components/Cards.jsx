import React from "react";

export default function Cards({ title, publishedAt, imageUsed }) {
  const date = new Date(publishedAt);
  const newDate = `${date
    .getDate()
    .toString()
    .padStart(2, "0")} ${date.toLocaleString("default", {
    month: "long",
  })} ${date.getFullYear()}`;
  const truncatedTitle =
    title.length > 55 ? title.substring(0, 55) + "..." : title;
  return (
    <div className="flex flex-col h-[25rem] max-w-xs rounded-xl overflow-hidden shadow-lg">
      <img
        className="object-cover w-full h-56"
        src={imageUsed}
        loading="lazy"
        alt="Sunset in the mountains"
      />
      <div className="flex-grow px-6 py-4">
        <div className="font-thin uppercase text-md opacity-80">{newDate}</div>
        <p className="pr-8 text-xl font-semibold text-gray-900">
          {truncatedTitle}
        </p>
      </div>
    </div>
  );
}
