import React from "react";
import { Parallax } from "react-parallax";

export default function Jumbotron() {
  return (
    <Parallax
      bgImage="https://suitmedia.static-assets.id/storage/files/601/6.jpg"
      strength={500}
    >
      <div className="relative max-w-screen-xl px-4 py-24 mx-auto text-center lg:py-56">
        <h1 className="mb-3 text-4xl font-semibold leading-none tracking-tight text-white md:text-5xl lg:text-6xl">
          Ideas
        </h1>
        <p className="mb-8 text-lg font-normal text-gray-300 lg:text-xl sm:px-16 lg:px-48">
          Where all our great things begin
        </p>
      </div>
      <div className="w-0 h-0 border-r-[100vw] border-r-transparent border-b-[13.9vw] border-white mt-[-13.9vw] transform scale-x-[-1] "></div>
    </Parallax>
  );
}
