import React, { useState, useEffect } from "react";
import logo from "../assets/logo.png";

export default function Header() {
  const [selectedButton, setSelectedButton] = useState("Ideas");
  const [showNavbar, setShowNavbar] = useState(true);
  const [prevScrollPos, setPrevScrollPos] = useState(0);

  const [isTop, setIsTop] = useState(true);
  const [isNavOpen, setIsNavOpen] = useState(false);
  const buttonNames = [
    "Work",
    "About",
    "Services",
    "Ideas",
    "Careers",
    "Contact",
  ];

  useEffect(() => {
    const handleScroll = () => {
      const currentScrollPos = window.pageYOffset;
      const visible = prevScrollPos > currentScrollPos;

      setPrevScrollPos(currentScrollPos);
      setShowNavbar(visible);
      setIsTop(currentScrollPos === 0);
    };

    window.addEventListener("scroll", handleScroll);

    return () => window.removeEventListener("scroll", handleScroll);
  }, [prevScrollPos]);

  const toggleNav = () => {
    console.log("Halo");
    setIsNavOpen(!isNavOpen);
  };

  return (
    <div>
      <nav
        className={`fixed z-50 w-full  bg-customOrange transition-all duration-300 ${
          showNavbar ? "translate-y-0" : "-translate-y-full"
        } ${isTop ? "bg-opacity-100" : "bg-opacity-75"} `}
      >
        <div className="flex flex-col flex-wrap items-center justify-between max-w-screen-xl p-6 mx-auto md:flex-row">
          <div className="flex justify-between w-full md:w-auto">
            <img src={logo} className="h-16 w-max-24 " alt="logo" />
            <div onClick={toggleNav}>
              <button
                data-collapse-toggle="navbar-default"
                type="button"
                className="inline-flex items-center justify-center w-10 h-10 p-2 text-sm text-white rounded-lg md:hidden "
                aria-controls="navbar-default"
                aria-expanded={isNavOpen}
              >
                <span className="sr-only">Open main menu</span>
                <svg
                  className="w-5 h-5"
                  aria-hidden="true"
                  xmlns="http://www.w3.org/2000/svg"
                  fill="none"
                  viewBox="0 0 17 14"
                >
                  <path
                    stroke="currentColor"
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeWidth="2"
                    d="M1 1h15M1 7h15M1 13h15"
                  />
                </svg>
              </button>
            </div>
          </div>
          <div
            className={`w-full  ${
              isNavOpen ? "flex flex-col" : "md:flex md:flex-row"
            } md:inline-flex md:w-auto ${isNavOpen ? "block" : "hidden"}`}
          >
            <ul className="flex flex-col w-full p-4 mt-4 font-medium border border-gray-100 rounded-lg sm:w-auto md:p-0 md:flex-row md:space-x-8 rtl:space-x-reverse md:mt-0 md:border-0 ">
              {buttonNames.map((name) => (
                <li key={name}>
                  <button
                    onClick={() => setSelectedButton(name)}
                    href="#"
                    className={`block py-2 px-3 text-white rounded  md:hover:bg-transparent md:border-0 md:p-0   `}
                  >
                    {name}
                  </button>
                  {selectedButton === name && (
                    <hr className="w-auto mt-2 border-2 border-white" />
                  )}
                </li>
              ))}
            </ul>
          </div>
        </div>
      </nav>
    </div>
  );
}
