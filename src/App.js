import "./App.css";
import Jumbotron from "./components/Jumbotron";
import Ideas from "./components/Ideas";

function App() {
  return (
    <div className="overflow-hidden">
      <Jumbotron />
      <Ideas />
    </div>
  );
}

export default App;
